# To do list

## Next release

1. finish mito processing pipeline (Cameron to agree on detail)
    1. configs/scaffolds need to be renamed in output to match input targets???
    2. needs a better warning if pipe-mtdna.ini fails ... and doesn't produce mtref.fa 
    3. how much sequences should mtref.fa hold to be considered successful
    4. what other output is needed?
1. check and improve reference pipeline - how well does it work? Is it properly documented?
1. review/update pipeline documentation (the omgtcp-docs subdirectory), 
especially the *maketree* process (and Cameron's QC using multiple compute nodes).
1. Cameron's QC work needs to be prepared for release

## general

1. better pipeline processing (improve *dorun* pipeline, move to *make*(1))
1. put reference files into their own directory with their index files and (symbolic) link their directories into sample directories. This means that index files need only be built once. (Use *make*(1) to build index files?)
2. Review whether Ansible files install should be via *git*(1) instead of a tar file.

# Specifics

## improve process submission

An advanced version would support (semi-)automatic sharing of processing between compute nodes.

## better *dorun*

Ideally, the *dorun* process should be more like *make*(1) in UNIX.

Currently, *dorun* uses .ini files and is more like a series of stages in a script.

It needs to be reliably restartable which means it avoids reprocessing completed results.

It differs from *make*(1) in that it can:

1. remove partial results (files that have some output but are not known to be a complete output from some pocess).
2. run processes in parallel, though these may need to be limited by the number of available theads
2. allow some subprocessed to fail when the input does not deliver output in an expected form

The challenge in our existing pipeline is the SPAdes assembly stage.
An assembly is attempted for each target sequence with sufficient reads identified
as being likely to be associated with a target.
Some of these assemblies fail so a script can't be definitive about what
outputs are needed.

My proposal is that the multiple assembly stages be finalised with a last step
that creates a file listing the successful assemblies ... and that file be treated as the output of the assembly stage.
There is also a directory or file that contains all the completed contigs or scaffolds.


===============================

Cameron's list of improvements:

1. Test using WORKDIR
1. use /opt/omg rather than ~/omg for code, data and prog directories?
1. Pipeline for when someone has a reference - rather than target protein files
1. Change to using MACSE? - yes! for Exon region only, Muscle is perfectly good for 5' and 3' regions.
1. 5' and 3' regions to go through final stage - At the moment the "mktree" stage always takes the exonic regions, but we need to also be able to do the same process with the 5' and 3' regions (which should be right-aligned and left aligned respectively)
1. Finish QC integration and testing - Cam to do
1. It would be good to see if we can have a simple install script for the local machine – it would safely ensure git is working on the system then git clone the latest tagged version of the ansible stuff. This would make distribution far simpler.
1. Check that there is a record of which software version were used to process a dataset … so and maybe comment on “repoducability”. We may need to be more explicit about the versions of software in the ansible install process than we are now.
1. Move from dorun.py to using make(1) for pipeline sections. This results in less complex software to maintain.
1. Currently, the shell scripts in the pipeline use set -x quite a bit. This produces a lot of extra output in the batch logs which sometimes makes it hard to spot where things went awry. Is thee a text file format and a text viewer that supports “folding” of logging (or probably more general text folding) that we could use for logging file format? - Can we compromise and have each pipeline section output a standard "tag" to search for in the log file?
1. Ancient DNA corrections/filters from Friday's speaker (Leonie) - Cam to do/provide
1. Implement final filter for phylo (from David) - Cam to do/provide
1. We need to do review all the documentation. I’d like to document how the pipeline works (is meant to work) and what implementation choices we used – diamond vs blast, spades vs other, bb-tools, …
1. Mitochondrial data - leave the pipeline for now, we might add some stuff at the end. Let's just make sure it doesn't crash.
1. Reference alignment script where you can choose the reference by name - this should in theory be no different to the QC script genomics DNA reference, though it's likely a whole genome (~3 GB size) rather than just a collection of targets. This would be used by the Reference Pipeline section (which normally follows the de novo stuff).
1. Script to let a user upload and index a reference - I'm not sure how you would want to do this. We can discuss.
1. Script for uploading user data in a distributed fashion - doesn't currently exist - Cam? Should be similar to other distributed tasks
1. Check GATK does realignment around indels - It may already do this in our pipeline, but I just want to check it - Cam to do.
1. Get remote email working (it seemed to be working but stopped - again).
