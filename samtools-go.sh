set -e 
cd ~/omg/prog/samtools-1.6
./configure
make
sudo make install

cd ~/omg/prog/bcftools
make
sudo make install
