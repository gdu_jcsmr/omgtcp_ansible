#!/bin/bash
cd ~/omg/data
refb=$PWD/MarsupialTargetsProt
bash ~/omg/code/fasplit.sh $PWD/mtp $refb.fa
~/omg/prog/diamond makedb -p 4 --in $refb.fa --db $refb.dmnd
refm=mito
~/omg/prog/diamond makedb -p 4 --in $refm.faa.gz --db $refm.dmnd
bwa index mito.fna.gz

