#!/bin/bash

# script to install OMG data and programs

#> ~/omg-go.log 2>&1
echo Starting
set -ex

cd
mkdir -p omg/data omg/prog

# unpack the files archive - could let ansible do this,
# but it's safer to do it here ... before the following commands run.

echo unpack files
cd omg
# get link from AARNET cloudstor - add /download to end of link
wget -O ~/omg-files.tgz https://cloudstor.aarnet.edu.au/plus/s/wpmxeqFer3BJgbf/download
tar -xzvf ~/omg-files.tgz
rm ~/omg-files.tgz

# define the data files
echo moving data files
dfs=(
    MarsupialTargetsProt.fa
    MarsupialTargetsDNA.fa
    BatTargetsProt.fa
    BatTargetsDNA.fa
    MitoDNA.fa.gz
    MitoProt.fa.gz
)

# move the data files
cd ~/omg/files

mv ${dfs[@]} ~/omg/data/.

progs=(
    # fastqc_v0.11.5.zip
    BBMap_37.50.tar.gz
    GenomeAnalysisTK-3.8-0.tar.bz2
    SPAdes-3.12.0-Linux.tar.gz
    # muscle3.8.31_i86linux64.tar.gz
)

[ -d FastQC ] && rm -r FastQC
# unpack program files
echo unpack program files
cd ~/omg/prog
mv ../files/picard.jar .
for fn in ${progs[*]}
do
  echo processing $fn
  case $fn in
    *.tar.gz|*.tgz) tar -xzf ../files/$fn ;;
    *.tar.bz2|*.tbz) tar -xjf ../files/$fn ;;
    *.zip) unzip -u ../files/$fn ;;
  esac
  rm ../files/$fn
done

# cd ~/omg/files
# echo building samtools
# tar -xzf samtools-1.6.tar.gz
# cd samtools-1.6
# ./configure
# make
# sudo make install
# 
# git clone git://github.com/samtools/htslib.git
# git clone git://github.com/samtools/bcftools.git
# cd ../bcftools
# echo building bcftools
# make
# sudo make install

rm -rf ~/omg/files

echo Done.

echo processing data
cd ~/omg/data
# refb=$PWD/MarsupialTargetsProt
# for refn in $PWD/*TargetsProt.fa
# do
#   refb=${refn%.fa}
#   bash ~/omg/code/fasplit.sh $PWD/mtp $refb.fa
#   ~/omg/prog/diamond makedb -p 4 --in $refb.fa --db $refb.dmnd
# done
refm=MitoProt
~/omg/prog/diamond makedb -p 4 --in $refm.fa.gz --db $refm.dmnd
bwa index MitoDNA.fa.gz

cd ~/omg/code
source param.sh
export WORKDIR=${WORKDIR:-/mnt/OMG}
for d in BATCH SUBMIT DONE
do
  mkdir -p $WORKDIR/$d
done

cat >cronjob.sh <<-CRONJOB
	#!/bin/bash
	# use crontab to make this run regularly
	# if no job is running, start one
	
	export WORKDIR=$WORKDIR
	cd \$WORKDIR
	
	[ -f BATCH/* ] && exit  # there should be zero or just one file in the BATCH Directory
	pending=(\$(ls -tr SUBMIT)) # submitted filenames in mtime order
	# create lock
	[ -n "\$pending" ] && until mkdir SUBMIT/lock ; do sleep 5; done 
	[ -d SUBMIT/lock ] && {
	[ -f SUBMIT/\$pending ] && mv SUBMIT/\$pending BATCH/.
	batch <<-CMDS
		set -x
		/bin/bash -l BATCH/\$pending || echo Job \$pending failed!
		[ -f BATCH/\$pending ] && mv BATCH/\$pending DONE/.
		CMDS
	rmdir SUBMIT/lock
	}
CRONJOB
chmod 755 cronjob.sh
