#!/usr/bin/env bash

# you can add more scripts for remote submission below
declare -A cmd=([samp]=run-omgpipeline.sh [qual]=run-sample-qual.sh)

die () { date ; echo "$@" ; exit 1 ; }

[ -n "$WORKDIR" ] || WORKDIR=/mnt/OMG

[ -n "$1" -a -n "$2" ] || die "usage: $0 command sample_id [node_ip] [bat|marsupial]
    command is one of: ${!cmd[@]}
    sample_id is an OMG sample identifier"
[ -n "${cmd[$1]}" ] || die "$1: command is unknown"

# check the IP is valid and find the keyfile
if [ -n "$3" ]
then
  eval $(grep $3 host-list | (read name ip x ; echo name=$name ip=$ip) )
  [ -n "$ip" ] || die Wrong/unknown IP = $3 not in host/node list.
  eval $(grep "^$ip" hosts | head -n 1 | cut -f3 -d' ')
else
  eval $(head -n 1 host-list | (read name ip x ; echo name=$name ip=$ip) )
  # default is primary node - first node in the hosts file
  eval $(grep "^[^[]" hosts | head -n 1 | cut -f3 -d' ')
  [ -n "$ansible_ssh_private_key_file" ] || die No default IP or node - this is very bad!
fi

keyfile=$ansible_ssh_private_key_file

dir=tmp
mkdir -p $dir

codedir='~/omg/code' # location of script on remote node
echo $codedir/${cmd[$1]} $2 $4 >$dir/$2

echo "submitting sample $2 to remote node $name ($ip)"
echo "for processing with script: ${cmd[$1]}"
scp -i $keyfile $dir/$2 ubuntu@$ip:$WORKDIR/SUBMIT/.
#rm $dir/$2
