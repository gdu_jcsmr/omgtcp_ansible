#!/usr/bin/env python

"""
    write a bash script to distribute sample qual runs, and all main pipeline runs across all available nodes
"""

import argparse
import sys
import csv

def read_hosts(hosts_fn):
    # read hosts list
    if hosts_fn is None:
        hosts_fn = 'hosts'
    hosts = []
    with open(hosts_fn, 'rt') as f:
        section_seen = False
        for line in f:
            if line.strip() == '':
                continue
            if not section_seen:
                if line.strip() == '[omg-node]':
                    section_seen = True
                continue
            else:
                ip = line.strip().split(' ')[0]
                hosts.append(ip)
    return hosts


def read_sample_list(sample_fn):
    samples = []
    with open(sample_fn, 'rt') as f:
        for line in f:
            if line.strip() == '':
                continue
            samples.append(line.strip())
    return samples


def read_bulk_download(bulk_fn):
    samples = []
    with open(bulk_fn, 'rt') as f:
        csv_reader = csv.reader(f, delimiter=',')
        fsid_col = 45
        for i, row in enumerate(csv_reader):
            if i == 0:
                for j, col in enumerate(row):
                    c = col.strip().lower()
                    if c == 'facility_sample_id':
                        fsid_col = j
                        print('FSID found in column', fsid_col, file=sys.stderr)
                        break
            else:
                samples.append(row[fsid_col])
    return samples


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ref', choices=['bat', 'marsupial'], help='Optional: reference target group')
    parser.add_argument('--bulk', help='Path to bulk download package metadata file (filename.csv)')
    parser.add_argument('--samples', help='Path to file containing list of facility_sample_id')
    parser.add_argument('--hosts', help='Path to non-standard hosts file')
    parser.add_argument('--expt', required=True, help='Experiment name to prefix to output scripts')
    args = parser.parse_args()

    if len(args.expt) == 0:
        print('You must give an experiment name to prefix to output files using --expt')
        exit(-1)

    if args.samples is not None:
        samples = read_sample_list(args.samples)
    elif args.bulk is not None:
        samples = read_bulk_download(args.bulk)
    else:
        print('Path to either sample list or to bulk download metadata file (in package_metadata dir) must be given')
        exit(-1)

    hosts = read_hosts(args.hosts)

    main_out_name = args.expt + '_submit_all_samp.sh'
    qual_out_name = args.expt + '_submit_all_qual.sh'
    with open(main_out_name, 'wt') as outm, open(qual_out_name, 'wt') as outq:
        for i,s in enumerate(samples):
            machine = i % len(hosts)
            ip = hosts[machine]
            outparts = ['./submit_qual.sh', s, ip]
            if args.ref:
                outparts.append(args.ref)
            outstr = ' '.join(outparts) + '\n'
            outq.write(outstr)
            outparts = ['./submit_samp.sh', s, ip]
            if args.ref:
                outparts.append(args.ref)
            outstr = ' '.join(outparts) + '\n'
            outm.write(outstr)


if __name__ == '__main__':
    main()
