#!/bin/bash

die () { date ; echo "$@" ; exit 1 ; }
[ -z "$WORKDIR" ] || WORKDIR=/mnt/OMG

[ -n "$1" ] || die usage: "$0 sample_id [node_ip] [bat|marsupial]"

# check the IP is valid and find the keyfile
if [ -n "$2" ]
then
  eval $(grep $2 host-list | (read name ip x ; echo name=$name ip=$ip) )
  [ -n "$ip" ] || die Wrong/unknown IP = $2 not in host/node list.
  eval $(grep "^$ip" hosts | head -n 1 | cut -f3 -d' ')
else
  # default is primary node - first node in the hosts file
  eval $(grep "^[^[]" hosts | head -n 1 | cut -f3 -d' ')
  [ -n "$ansible_ssh_private_key_file" ] || die No default IP or node - this is very bad!
fi

keyfile=$ansible_ssh_private_key_file

dir=tmp
mkdir -p $dir

codedir='~/omg/code' # location of script on remote node
echo $codedir/run-omgpipeline.sh $1 $3 >$dir/$1

echo "submitting sample $1 to remote node $name ($ip) for de novo assembly and haplotype calling."
scp -i $keyfile $dir/$1 ubuntu@$ip:$WORKDIR/SUBMIT/.
