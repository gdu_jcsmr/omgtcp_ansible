#!/bin/bash
codedir=~/omg/code
log=batch.log
# TODO: make the $tccpkeypath available
keypath=${tccpkeypath:-~/.ssh/omg_key.pem}

die () { date ; echo "$@" ; exit 1 ; }

[ -n "$2" ] || die usage: $0 sample_ID node_IP

dir=tmp
[ -d tmp ] || mkdir tmp
s=$1

topdir=${WORKDIR:-/mnt/OMG}
codedir='~/omg/code'

cat >$dir/$s <<-CMDS
	# use /bin/sh syntax for die() function!
	die () { date ; echo \"\$@\" ; exit 1 ; }

	set -e
	cd $topdir
	[ -d $s ] || mkdir $s || exit
        cd $s
	{ 
		date
		$codedir/run-qc.sh . || \
		  die "$s: QC pipeline failed!"
		date
		$codedir/pipe-rawmt.ini . || \
		  die "$s: rawmt pipeline failed!"
		date
	} >$log 2>&1
	echo Sample $s done. | mail -s "OMG: Sample $s done." -A $log \$(cat ~/.forward)
	CMDS

echo submitting $s to remote node $2
scp -i $keypath $dir/$s $2:$topdir/SUBMIT/.
