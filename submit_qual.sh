#!/bin/bash

### submit_sample_qual.sh
#
# Use this script to submit samples for quality assessment
# Follow up with submit_collect_quals.sh to gather results and email reports
#
# usage: ./submit_sample_qual.sh facility_sample_id node_ip_adress [target set]
# [target set] can be 'bat' or 'marsupial'. It defaults to 'marsupial'
#
###

die () { date ; echo "$@" ; exit 1 ; }

[ -n "$1" ] || die usage: "$0 sample_id [node_ip]"

[ -z "$2" ] || eval $(grep $2 host-list | (read name ip x ; echo name=$name ip=$ip) )
[ -z "$ip" ] && ip='[^[]'	# get first node if none are specified
eval $(grep "^$ip" hosts | cut -f3 -d' ')
keyfile=$ansible_ssh_private_key_file

# echo node=$name keyfile=$keyfile

dir=tmp
mkdir -p $dir

codedir='~/omg/code' # location of script on remote node
echo $codedir/run-sample-qual.sh $1 $3 >$dir/$1

echo "submitting sample $1 to remote node $name ($ip) for quality assessment."
scp -i $keyfile $dir/$1 ubuntu@$ip:/mnt/OMG/SUBMIT/.
